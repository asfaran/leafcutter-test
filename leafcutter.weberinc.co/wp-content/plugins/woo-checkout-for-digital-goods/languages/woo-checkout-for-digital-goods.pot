#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-10-01 16:11+0530\n"
"PO-Revision-Date: 2018-05-25 13:01+0530\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.3\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html__\n"
"Last-Translator: \n"
"Language: en_US\n"
"X-Poedit-SearchPath-0: languages\n"
"X-Poedit-SearchPath-1: .\n"

#: admin/class-woo-checkout-for-digital-goods-admin.php:94
msgid "DotStore Plugins"
msgstr ""

#: admin/class-woo-checkout-for-digital-goods-admin.php:256
#, php-format
msgid "%1$s (#%2$s)"
msgstr ""

#: admin/class-woo-checkout-for-digital-goods-admin.php:349
#: admin/class-woo-checkout-for-digital-goods-admin.php:361
#: admin/class-woo-checkout-for-digital-goods-admin.php:373
#: admin/class-woo-checkout-for-digital-goods-admin.php:399
msgid "true"
msgstr ""

#: admin/partials/header/plugin-header.php:19
msgid "Premium Version"
msgstr ""

#: admin/partials/header/plugin-header.php:61
msgid "General Setting"
msgstr ""

#: admin/partials/header/plugin-header.php:64
#: public/class-woo-checkout-for-digital-goods-public.php:177
#: public/class-woo-checkout-for-digital-goods-public.php:263
#: public/class-woo-checkout-for-digital-goods-public.php:288
#: public/class-woo-checkout-for-digital-goods-public.php:330
msgid "Quick Checkout"
msgstr ""

#: admin/partials/header/plugin-header.php:67
msgid "About Plugin"
msgstr ""

#: admin/partials/header/plugin-header.php:69
#: admin/partials/wcdg-get-started-page.php:17
msgid "Getting Started"
msgstr ""

#: admin/partials/header/plugin-header.php:70
#: admin/partials/wcdg-information-page.php:13
msgid "Quick info"
msgstr ""

#: admin/partials/header/plugin-header.php:74
msgid "Dotstore"
msgstr ""

#: admin/partials/header/plugin-header.php:76
msgid "WooCommerce Plugins"
msgstr ""

#: admin/partials/header/plugin-header.php:77
msgid "Wordpress Plugins"
msgstr ""

#: admin/partials/header/plugin-header.php:78
msgid "Contact Support"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:18
msgid "Important link"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:23
msgid "Plugin documentation"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:27
msgid "Support platform"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:31
msgid "Suggest A Feature"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:35
msgid "Changelog"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:42
msgid "OUR POPULAR PLUGINS"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:47
msgid "Advanced Flat Rate Shipping Method"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:51
msgid "WooCommerce Conditional Product Fees"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:55
msgid "Advance Menu Manager"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:59
msgid "Woo Enhanced Ecommerce Analytics Integration"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:63
msgid "Advanced Product Size Charts"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:67
msgid "WooCommerce Blocker – Prevent Fake Orders"
msgstr ""

#: admin/partials/header/plugin-sidebar.php:72
msgid "VIEW ALL"
msgstr ""

#: admin/partials/wcdg-general-setting.php:11
#: admin/partials/wcdg-quick-checkout__premium_only.php:14
msgid "Setting"
msgstr ""

#: admin/partials/wcdg-general-setting.php:46
msgid "Enable / Disable"
msgstr ""

#: admin/partials/wcdg-general-setting.php:54
msgid "Enable or Disable Digital Goods for WooCommerce Checkout Plugin"
msgstr ""

#: admin/partials/wcdg-general-setting.php:60
msgid "Select Field On Checkout"
msgstr ""

#: admin/partials/wcdg-general-setting.php:84
msgid "Select fields which you want to exclude on checkout page."
msgstr ""

#: admin/partials/wcdg-general-setting.php:90
msgid "Order Note Enable"
msgstr ""

#: admin/partials/wcdg-general-setting.php:97
msgid "Enable Order Note which you want to exclude on checkout page."
msgstr ""

#: admin/partials/wcdg-general-setting.php:106
msgid "Quick Checkout Button display on Shop page"
msgstr ""

#: admin/partials/wcdg-general-setting.php:113
msgid "Display Quick Checkout Button on Shop Page for Digital Product"
msgstr ""

#: admin/partials/wcdg-general-setting.php:119
msgid "Quick Checkout Button display on Product Details page"
msgstr ""

#: admin/partials/wcdg-general-setting.php:126
msgid ""
"Display Quick Checkout Button on Product Details Page for Digital Product"
msgstr ""

#: admin/partials/wcdg-general-setting.php:132
msgid "Quick Checkout On"
msgstr ""

#: admin/partials/wcdg-general-setting.php:134
msgid "Quick Checkout for all downloadable and/or virtual products"
msgstr ""

#: admin/partials/wcdg-general-setting.php:135
msgid "Manually Quick Checkout List for Product/Category/Tag "
msgstr ""

#: admin/partials/wcdg-general-setting.php:135
msgid "Click Here"
msgstr ""

#: admin/partials/wcdg-general-setting.php:146
msgid "Save"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:12
msgid "Digital Goods for WooCommerce Checkout"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:18
msgid "Create/manage multiple shipping rules as per your needs."
msgstr ""

#: admin/partials/wcdg-get-started-page.php:20
msgid "Step 1:"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:20
msgid "Setup Shipping Method Configuration with Shipping Method Rules"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:26
msgid "Step 2:"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:26
msgid "You can see list of all shipping methods."
msgstr ""

#: admin/partials/wcdg-get-started-page.php:32
msgid "Step 3:"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:32
msgid "Enable shipping method on the cart/checkout page if rule is satisfied"
msgstr ""

#: admin/partials/wcdg-get-started-page.php:37
msgid "Important Note: "
msgstr ""

#: admin/partials/wcdg-get-started-page.php:37
msgid "This plugin is only compatible with WooCommerce version 2.4.0 and more"
msgstr ""

#: admin/partials/wcdg-information-page.php:17
msgid "Product Type"
msgstr ""

#: admin/partials/wcdg-information-page.php:18
msgid "WooCommerce Plugin"
msgstr ""

#: admin/partials/wcdg-information-page.php:21
msgid "Product Name"
msgstr ""

#: admin/partials/wcdg-information-page.php:25
msgid "Installed Version"
msgstr ""

#: admin/partials/wcdg-information-page.php:26
msgid "Free Version"
msgstr ""

#: admin/partials/wcdg-information-page.php:29
msgid "License & Terms of use"
msgstr ""

#: admin/partials/wcdg-information-page.php:30
msgid "Click here"
msgstr ""

#: admin/partials/wcdg-information-page.php:30
msgid " to view license and terms of use."
msgstr ""

#: admin/partials/wcdg-information-page.php:33
msgid "Help & Support"
msgstr ""

#: admin/partials/wcdg-information-page.php:36
msgid "Quick Start"
msgstr ""

#: admin/partials/wcdg-information-page.php:37
msgid "Guide Documentation"
msgstr ""

#: admin/partials/wcdg-information-page.php:38
msgid "Support Forum"
msgstr ""

#: admin/partials/wcdg-information-page.php:43
msgid "Localization"
msgstr ""

#: admin/partials/wcdg-information-page.php:44
msgid "English, German"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:30
msgid "Successfully Insert Product..!!"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:32
msgid "Please select at least one product..!!"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:39
msgid "Select Digital Product"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:42
msgid "Add Product"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:43
#: admin/partials/wcdg-quick-checkout__premium_only.php:74
#: admin/partials/wcdg-quick-checkout__premium_only.php:105
msgid "Back"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:61
msgid "Successfully Insert Category..!!"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:63
msgid "Please select at least one category..!!"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:70
msgid "Select Category"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:73
msgid "Add Category"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:92
msgid "Successfully Insert Tag..!!"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:94
msgid "Please select at least one tag..!!"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:101
msgid "Select Tag"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:104
msgid "Add Tag"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:118
msgid "Products"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:119
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-category.php:87
msgid "Categories"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:120
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-tag.php:87
msgid "Tags"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:127
msgid "Digital Products List"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:128
#: admin/partials/wcdg-quick-checkout__premium_only.php:179
#: admin/partials/wcdg-quick-checkout__premium_only.php:228
msgid "Add new"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:129
#: admin/partials/wcdg-quick-checkout__premium_only.php:180
#: admin/partials/wcdg-quick-checkout__premium_only.php:229
msgid "Delete ( Selected )"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:135
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-product.php:87
msgid "Product"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:136
#: admin/partials/wcdg-quick-checkout__premium_only.php:188
#: admin/partials/wcdg-quick-checkout__premium_only.php:237
msgid "Action"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:161
#: admin/partials/wcdg-quick-checkout__premium_only.php:211
#: admin/partials/wcdg-quick-checkout__premium_only.php:260
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-category.php:55
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-product.php:55
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-tag.php:55
msgid "Edit"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:162
#: admin/partials/wcdg-quick-checkout__premium_only.php:212
#: admin/partials/wcdg-quick-checkout__premium_only.php:261
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-category.php:56
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-product.php:56
#: includes/wp-list-table/class-woo-checkout-for-digital-goods-wp-list-table-tag.php:56
msgid "Delete"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:178
msgid "Categories List"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:187
msgid "Category"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:227
msgid "Tags List"
msgstr ""

#: admin/partials/wcdg-quick-checkout__premium_only.php:236
msgid "Tag"
msgstr ""

#: public/class-woo-checkout-for-digital-goods-public.php:362
msgid ""
"Please check your email for login details and update your remaining billing "
"details."
msgstr ""

#: public/class-woo-checkout-for-digital-goods-public.php:365
msgid "My Account"
msgstr ""
