<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'webehqfd_lctest' );

/** MySQL database username */
define( 'DB_USER', 'webehqfd_lctest' );

/** MySQL database password */
define( 'DB_PASSWORD', '.17@S1qXpd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7ropfppbewerkaghlhq1pgcci0p62axnt7p6woccnubbv7v5numaaivwme4py2vq' );
define( 'SECURE_AUTH_KEY',  '1cyjkwzm5qinh1zm5pb4akgmejbp9aw401fuq3l70xi3m2gekpnfov0mgi37067h' );
define( 'LOGGED_IN_KEY',    'm1icjaqgan01zncknngcxwbowsaswdwkhxoxmxqhixzggk6zbx4gzdhasjnlxewn' );
define( 'NONCE_KEY',        'aewaacyptyfbnf4zwx1fpkyoklphn9ifd3bdukonocgitubi6tv9r54sworct9lf' );
define( 'AUTH_SALT',        'dwh4kvu3tvplhetel6e2qqt4b8ucap9jqaqewvvzdrmpg7krzpwtsbed7lngdcbw' );
define( 'SECURE_AUTH_SALT', 'd8eyks12d4tngbfrxk0hwwujciojxbkxmouejxmmbg01oy0fjphufpb3vlybnxod' );
define( 'LOGGED_IN_SALT',   '2cotyzl7lsjijkhqpdnd08r3qvhrwqsdnezc9c3fyhn9ehk439tsc73bxp18rtwo' );
define( 'NONCE_SALT',       'fzzzxouwkr4gmndzfqpqoql1vuyn9pdclikorztty7lqn0rdqg4eisgfdh7uaoyt' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
