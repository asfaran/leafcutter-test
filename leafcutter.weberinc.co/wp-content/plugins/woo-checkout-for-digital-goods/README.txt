=== Digital Goods for WooCommerce Checkout ===
Plugin Name: Digital Goods for WooCommerce Checkout
Plugin URI: https://wordpress.org/plugins/woo-checkout-for-digital-goods/
Author: Thedotstore
Author URI: https://www.thedotstore.com/
Contributors: dots, niravcse006
Stable tag: 3.2
Tags: Woocommerce Checkout, Digital Goods, remove billing fields, checkout fields, Checkout for Digital Goods.
Requires at least: 3.8
Tested up to: 5.2.1
Donate link: 
Copyright: (c) 2015-2019 Thedotstore all rights reserved (support@thedotstore.com)
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin will remove billing address fields for downloadable and virtual products.

== Description ==
Digital Goods for WooCommerce Checkout allows you to skip the unnecessary fields so order can place faster.

If you are selling downloadable digital products then you might not need customer's billing and shipping address and customers expect to get the product as quickly as possible.

This plugin helps to remove unnecessary fields from checkout page and make process smooth and easy for customer. No settings required just activate and that's it.

Pro Plugin Demo : <a href ="http://woodigitalgoods.demo.store.multidots.com">View Demo</a>

= Key Features: =

* Exclude Fields during Checkout You can eliminate any number of fields from the checkout form - for your products, categories, and tags (For downloadable & Digital Product)
* Quick Checkout Button display on the Shop page
* Quick Checkout Button display on the Product Details page
* Enable or disable exclude order note on checkout 
* Quick Checkout for all downloadable and/or virtual products

= Benefits of Using this Plugin: =

* Significant Decrease in Time Taken in Checkout

With the lesser amount of information to fill and only essential steps to be taken for checkout, you reduce the time taken in the checkout process by your customers to a bare minimum. Sure, it will make them happy!

* Lesser Frustration and Easy Checkout

Though physical product deliveries require a significant amount of data from the customer, things are not the same in the case of digital products. So, just ask for information that is necessary and let the buyers download/use the product.

* Reduced Cart Abandonment Rate

By putting dozens of information fields on checkout form for digital products, you’re just making sure that your buyer gets frustrated and discards the process. Do not do that. Reduce their hassles and avoid order abandoning.

* Increased Sales of your Digital Goods

Quick checkouts and better user experience encourage people to purchase digital goods from you. So whenever you are selling a virtual or downloadable product, do not push your buyers to follow the same process as physical products.

<blockquote>

= Digital Goods For WooCommerce Checkout Pro version: =

Need even more? upgrade to <a href="https://www.thedotstore.com/woocommerce-checkout-for-digital-goods/">Digital Goods For WooCommerce Checkout Pro</a> version and get access to all extra features available in Digital Goods For WooCommerce Checkout Pro.

= Digital goods on Quick checkout =
<ol>
<li>Items in the “Quick Checkout List” only</li>
<li>Customers fill in the additional fields after the payment</li>
<li>Delayed account creation</li>
<li>Restrict with User Role	</li>
</ol>
</blockquote>

= You can check our other plugins: =

<ol>
<li> <a href ="http://www.thedotstore.com/advanced-flat-rate-shipping-method-for-woocommerce">Advance Flat Rate Shipping Method For WooCommerce</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-blocker-lite-prevent-fake-orders-blacklist-fraud-customers/">Blocker – Prevent Fake Orders And Blacklist Fraud Customers for WooCommerce </a></li>
<li> <a href ="http://www.thedotstore.com/woocommerce-enhanced-ecommerce-analytics-integration-with-conversion-tracking">Enhanced Ecommerce Google Analytics Plugin for WooCommerce</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-category-banner-management/">Category Banner Management for Woocommerce</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-conditional-product-fees-checkout/">Conditional Product Fees For WooCommerce Checkout</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-advanced-product-size-charts/">Advanced Product Size Charts for WooCommerce</a></li>
<li> <a href ="http://www.thedotstore.com/advance-menu-manager-wordpress/">Advance Menu Manager for WordPress</a></li>
</ol>

== Installation ==

= Minimum Requirements =

* WooCommerce 3.0 or higher

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of Digital Goods for WooCommerce Checkout, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type Digital Goods for WooCommerce Checkout and click Search Plugins. Once you've found our plugin you can view details about it such as the the rating and description. Most importantly, of course, you can install it by simply clicking Install Now�?.

= Manual Installation =

1. Unzip the files and upload the folder into your plugins folder (/wp-content/plugins/) overwriting older versions if they exist
2. Activate the plugin in your WordPress admin area.

== Screenshots ==
1. Digital Goods for WooCommerce Checkout Screenshot 1.
2. Digital Goods for WooCommerce Checkout Screenshot 2.
3. Digital Goods for WooCommerce Checkout Screenshot 3.
4. Digital Goods for WooCommerce Checkout Screenshot 4.

== Frequently Asked Questions ==

= Q. In which WordPress version this Plugin is compatible? =
Ans: It is compatible from 3.8 to 5.3 WordPress version.

= Q.In which WooCommerce version this Plugin is compatible? =
Ans: It is compatible for 2.6 and greater than WooCommerce plugin

= Q. Is this plugin able to detect digital goods from my e-commerce store? =
Ans: Yes. If you will enable the plugin for all the digital goods, it will be deployed for all virtual and downloadable products that do not involve shipping.

= Q. Does WooCommerce Checkout for Digital Goods Plugin work for multilingual websites (or WPML)? =
Ans: As of now, the plugin is not WPML-ready.

= Q. May I enable quick checkout for B2B clients while keeping it normal for the B2C clients? =
Ans: Yes, it is easy to do so while you use our plugin. All you need to do is – choose the user role assigned to B2B clients and configure the quick checkout settings for them. Save settings to imply the changes.

= Q. How many fields can be excluded from the checkout process? =
Ans: We understand that different sellers have different requirements. So, we have not kept any limit on the number of fields. You can exclude as many form fields from the checkout process, according to your store’s needs.

= Q. Can I disable different sets of fields for a different set of product/categories/tags? =
Ans: No, it is not yet possible through this plugin. It lets you apply store-wide settings.

== Upgrade Notice ==

Automatic updates should work great for you.  As always, though, we recommend backing up your site prior to making any updates just to be sure nothing goes wrong.

== Changelog ==

= 3.2 - 10-01-2020 =
* Fixed: "Ship to a different address" form is not displaying.

= 3.1 - 20-12-2019 =
* Fixed: "Additional Information" title display issue.

= 3.0 - 10-12-2019 =
* New - Quick checkout button display on Shop page
* New - Quick checkout button display on Single Product Page
* New - Quick checkout on : All downloadable and/or virtual products
* New - Layout Structure
* Update - VIP minimum
* Compatible with Freemius

= 2.9 – 19-11-2019 =
* Compatible with WordPress 5.3.x
* Compatible with WooCommerce 3.8.x

= 2.8 - 20-09-2019 =
* Maintenance Releases

= 2.7 - 28-04-2019 =
* Compatible with WordPress Version 5.2.x and WooCommerce version 3.6.x

= 2.6 - 27-03-2019 =
* Normal bug fixed.

= 2.5 - 15-03-2019 =
* Only virtual products then not apply issue fixed.

= 2.4 - 28-01-2019 =
* Normal bug fixed.

= 2.3 - 06-08-2018 =
* Normal bug fixed.

= 2.2 - 28-05-2018 =
* Fixed vulnerable code issue
* Compatible with WordPress 4.9.x and WooCommerce 3.4.x

= 2.1 - 25-05-2018 =
* Normal bug fix.
* Check Wordpress 4.9.6 compatibility

= 2.0 - 19-04-2018 =
* Normal bug fixed.

= 1.9 - 13-06-2017 =
* FIX:Variable Digital Product Issue resolved.
* Check Wordpress 4.8 compatibility

= 1.8 - 11-05-2017 = 
* FIX:both product available that time required shipping info issue resolved.

= 1.7 - 14-04-2017 = 
* Normal bug fix.
* Check Wordpress and WooCommerce 3.0.x compatibility

= 1.6 - 04-04-2017 = 
* Normal bug fix.

= 1.5 - 23-03-2017 = 
* Normal bug fix.
* Downloadable Product functionality issue resolve.

= 1.4 - 24-12-2016 = 
* Check Wordpress and WooCommerce compatibility

= 1.3 - 26-08-2016 = 
* Check Wordpress and WooCommerce compatibility

= 1.2 - 11.07.2016 =
* Fix - Fixed minor bug.
* New - Subscription form added.

= 1.1.1 - 04.05.2016 =
* Fix - Fixed PHP error notice.

= 1.1 - 02.05.2016 =
* New - Settings page added for select fields which you want to exclude from checkout page.

= 1.0.1 - 30.11.2015 =
* Tweak - Remote request handles on activate.

